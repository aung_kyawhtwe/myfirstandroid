package com.example.apptest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
    Button btnsave;
    EditText edtname,edtemail,edtphone;
    RadioButton rdbmale,rdbfemale;
    CheckBox cbbox;
    String gender="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String name=edtname.getText().toString();
                final String email=edtemail.getText().toString();
                final String phone=edtphone.getText().toString();
                if(rdbmale.isChecked())
                    gender="Male";
                if(rdbfemale.isChecked())
                    gender="Female";

                Info info=new Info(name,email,phone,gender);
                Intent intent=new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("Object", info);
                startActivity(intent);
            }
        });

    }
    public void initialize(){
        edtname=findViewById(R.id.edt_name);
        edtemail=findViewById(R.id.edt_eamil);
        edtphone=findViewById(R.id.edt_phone);
        btnsave=findViewById(R.id.btn_save);
        rdbmale=findViewById(R.id.rdbm);
        rdbfemale=findViewById(R.id.rdbf);
        cbbox=findViewById(R.id.cbtn);
    }

}
